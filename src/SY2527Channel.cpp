static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/Instrumentation/SY2527Channel/src/SY2527Channel.cpp,v 1.9 2010-03-26 09:58:35 vince_soleil Exp $";
//+=============================================================================
//
// file :         SY2527Channel.cpp
//
// description :  C++ source for the SY2527Channel and its commands. 
//                The class is derived from Device. It represents the
//                CORBA servant object which will be accessed from the
//                network. All commands which can be executed on the
//                SY2527Channel are implemented in this file.
//
// project :      TANGO Device Server
//
// $Author: vince_soleil $
//
// $Revision: 1.9 $
//
// $Log: not supported by cvs2svn $
// Revision 1.8  2007/11/19 09:12:41  sebleport
// - fault and alarm state have got now a higher priority
//
// Revision 1.7  2007/10/15 15:40:42  sebleport
// - in get_device_property(), we write properties in database only if they have never been written
// - slotNumber and channelNumber are described in status attribute
//
// Revision 1.6  2007/09/07 13:14:56  sebleport
// - add position_in_crate (vector) data member
//
// Revision 1.5  2007/09/06 09:21:12  sebleport
// - shiftVoltage changed by deltaVoltage attribute
// - add OFF state
// - RUNNING state priority has changed
//
// Revision 1.4  2007/09/06 09:13:44  sebleport
// - shiftVoltage changed by deltaVoltage attribute
// - add OFF state
// - RUNNING state priority has changed
//
// Revision 1.3  2007/09/05 14:02:15  sebleport
// filling array argin command  corrected
//
// Revision 1.2  2007/09/05 09:41:37  sebleport
// the command and attribute code has been implemeted
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================



//===================================================================
//
//	The following table gives the correspondance
//	between commands and method's name.
//
//  Command's name|  Method's name
//	----------------------------------------
//  State     |  dev_state()
//  Status    |  dev_status()
//  PowerON   |  power_on()
//  PowerOFF  |  power_off()
//
//===================================================================


#include <tango.h>
#include <SY2527Channel.h>
#include <SY2527ChannelClass.h>

namespace SY2527Channel_ns
{
    
    //+----------------------------------------------------------------------------
    //
    // method : 		SY2527Channel::SY2527Channel(string &s)
    // 
    // description : 	constructor for simulated SY2527Channel
    //
    // in : - cl : Pointer to the DeviceClass object
    //      - s : Device name 
    //
    //-----------------------------------------------------------------------------
    SY2527Channel::SY2527Channel(Tango::DeviceClass *cl,string &s)
        :Tango::Device_4Impl(cl,s.c_str())
    {
        init_device();
    }
    
    SY2527Channel::SY2527Channel(Tango::DeviceClass *cl,const char *s)
        :Tango::Device_4Impl(cl,s)
    {
        init_device();
    }
    
    SY2527Channel::SY2527Channel(Tango::DeviceClass *cl,const char *s,const char *d)
        :Tango::Device_4Impl(cl,s,d)
    {
        init_device();
    }
    //+----------------------------------------------------------------------------
    //
    // method : 		SY2527Channel::delete_device()
    // 
    // description : 	will be called at device destruction or at init command.
    //
    //-----------------------------------------------------------------------------
    void SY2527Channel::delete_device()
    {
        //	Delete device's allocated object
        
        DELETE_SCALAR_ATTRIBUTE(attr_voltage_read);
        position_in_crate.clear();

    }
    
    //+----------------------------------------------------------------------------
    //
    // method : 		SY2527Channel::init_device()
    // 
    // description : 	will be called at device initialization.
    //
    //-----------------------------------------------------------------------------
    void SY2527Channel::init_device()
    {
        INFO_STREAM << "SY2527Channel::SY2527Channel() create device " << device_name << endl;
        
        // Initialise variables to default values
        //--------------------------------------------
        SY2527_crate_proxy = 0;
        message="";

        get_device_property();
        
        CREATE_SCALAR_ATTRIBUTE(attr_voltage_read);
        
        // initialise array of parameters for Proxy commands
        position_in_crate.push_back(slotNumber);
        position_in_crate.push_back(channelNumber);
    }
    
    //+----------------------------------------------------------------------------
    //
    // method : 		SY2527Channel::get_device_property()
    // 
    // description : 	Read the device properties from database.
    //
    //-----------------------------------------------------------------------------
    void SY2527Channel::get_device_property()
    {
        //	Initialize your default values here (if not done with  POGO).
        //------------------------------------------------------------------
        
        // defaut values are impossible to oblige user to define correct values
        
        const long IMPOSSIBLE_VALUE = -1;

        slotNumber = IMPOSSIBLE_VALUE;
        slotNumber = IMPOSSIBLE_VALUE;

        //	Read device properties from database.(Automatic code generation)
        //------------------------------------------------------------------
        Tango::DbData	dev_prop;
        dev_prop.push_back(Tango::DbDatum("SlotNumber"));
        dev_prop.push_back(Tango::DbDatum("ChannelNumber"));
        dev_prop.push_back(Tango::DbDatum("SY2527CrateProxyName"));
        
        //	Call database and extract values
        //--------------------------------------------
        if (Tango::Util::instance()->_UseDb==true)
            get_db_device()->get_property(dev_prop);
        Tango::DbDatum	def_prop, cl_prop;
        SY2527ChannelClass	*ds_class =
            (static_cast<SY2527ChannelClass *>(get_device_class()));
        int	i = -1;
        
        //	Try to initialize SlotNumber from class property
        cl_prop = ds_class->get_class_property(dev_prop[++i].name);
        if (cl_prop.is_empty()==false)	cl_prop  >>  slotNumber;
        //	Try to initialize SlotNumber from default device value
        def_prop = ds_class->get_default_device_property(dev_prop[i].name);
        if (def_prop.is_empty()==false)	def_prop  >>  slotNumber;
        //	And try to extract SlotNumber value from database
        if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  slotNumber;
        
        //	Try to initialize ChannelNumber from class property
        cl_prop = ds_class->get_class_property(dev_prop[++i].name);
        if (cl_prop.is_empty()==false)	cl_prop  >>  channelNumber;
        //	Try to initialize ChannelNumber from default device value
        def_prop = ds_class->get_default_device_property(dev_prop[i].name);
        if (def_prop.is_empty()==false)	def_prop  >>  channelNumber;
        //	And try to extract ChannelNumber value from database
        if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  channelNumber;
        
        //	Try to initialize SY2527CrateProxyName from class property
        cl_prop = ds_class->get_class_property(dev_prop[++i].name);
        if (cl_prop.is_empty()==false)	cl_prop  >>  sY2527CrateProxyName;
        //	Try to initialize SY2527CrateProxyName from default device value
        def_prop = ds_class->get_default_device_property(dev_prop[i].name);
        if (def_prop.is_empty()==false)	def_prop  >>  sY2527CrateProxyName;
        //	And try to extract SY2527CrateProxyName value from database
        if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  sY2527CrateProxyName;
        
        
        
        //	End of Automatic code generation
        //------------------------------------------------------------------
        
        // automatic generation property in the database if no declared under jive
        Tango::DbData db_d;
        Tango::DbDatum SlotNum("SlotNumber");
        Tango::DbDatum ChNum("ChannelNumber");
        Tango::DbDatum SY2527CratePrName("SY2527CrateProxyName");
        
        // default slot number
        if(slotNumber==IMPOSSIBLE_VALUE)
        {
            //the property has not been declared in the database
            SlotNum << IMPOSSIBLE_VALUE;
            db_d.push_back(SlotNum);
            message += "SlotNum property is missing ! \n";
        }
        
        // default channel number
        if(channelNumber==IMPOSSIBLE_VALUE)
        {
            // idem
            ChNum << IMPOSSIBLE_VALUE;
            db_d.push_back(ChNum);
            message += " channelNumber property is missing ! \n";
        }
        
        // default sY2527Crate Proxy Name
        if(sY2527CrateProxyName.empty()==true)
        {
            // idem
            SY2527CratePrName << "none";
            db_d.push_back(SY2527CratePrName);
            message += "sY2527CrateProxyName property is missing !\n";
        }
        
        // write not declared propreties in database.
        get_db_device()->put_property(db_d);        
}
//+----------------------------------------------------------------------------
//
// method : 		SY2527Channel::always_executed_hook()
// 
// description : 	method always executed before any command is executed
//
//-----------------------------------------------------------------------------
void SY2527Channel::always_executed_hook()
{
    if(SY2527_crate_proxy==0)
    {
            SY2527_crate_proxy = new Tango::DeviceProxyHelper(this->sY2527CrateProxyName,this);
            
            if(SY2527_crate_proxy==0)
            {
                Tango::Except::throw_exception(
                    static_cast<const char*>("OUT_OF_MEMORY"),
                    static_cast<const char*>("could not create SY2527 device proxy"),
                    static_cast<const char*>("SY2527Channel::always_executed_hook")
                    );
            }

    }
}
//+----------------------------------------------------------------------------
//
// method : 		SY2527Channel::read_attr_hardware
// 
// description : 	Hardware acquisition for attributes.
//
//-----------------------------------------------------------------------------
void SY2527Channel::read_attr_hardware(vector<long> &attr_list)
{
    DEBUG_STREAM << "SY2527Channel::read_attr_hardware(vector<long> &attr_list) entering... "<< endl;
    //	Add your own code here
}
//+----------------------------------------------------------------------------
//
// method : 		SY2527Channel::read_deltaVoltage
// 
// description : 	Extract real attribute values for deltaVoltage acquisition result.
//
//-----------------------------------------------------------------------------
void SY2527Channel::read_deltaVoltage(Tango::Attribute &attr)
{
    DEBUG_STREAM << "SY2527Channel::read_deltaVoltage(Tango::Attribute &attr) entering... "<< endl;
}

//+----------------------------------------------------------------------------
//
// method : 		SY2527Channel::write_deltaVoltage
// 
// description : 	Write deltaVoltage attribute values to hardware.
//
//-----------------------------------------------------------------------------
void SY2527Channel::write_deltaVoltage(Tango::WAttribute &attr)
{
    DEBUG_STREAM << "SY2527Channel::write_deltaVoltage(Tango::WAttribute &attr) entering... "<< endl;
    
    attr.get_write_value(attr_deltaVoltage_write);
    
    double presetVoltage = attr_voltage_write + attr_deltaVoltage_write;
    
    vector <double> set_delta_voltage_array;
    
    set_delta_voltage_array.push_back(slotNumber);
    set_delta_voltage_array.push_back(channelNumber);
    set_delta_voltage_array.push_back(presetVoltage);
    
    try
    {
        SY2527_crate_proxy->command_in("SetVoltageOnChannel",set_delta_voltage_array);
    }
    catch(Tango::DevFailed &df)
    {
        if(SY2527_crate_proxy!=0)
        {
            delete SY2527_crate_proxy;
            SY2527_crate_proxy = 0;
        }
        
        ERROR_STREAM<<df<<endl;
        throw df;
    }
}


//+----------------------------------------------------------------------------
//
// method : 		SY2527Channel::read_voltage
// 
// description : 	Extract real attribute values for voltage acquisition result.
//
//-----------------------------------------------------------------------------
void SY2527Channel::read_voltage(Tango::Attribute &attr)
{
    DEBUG_STREAM << "SY2527Channel::read_voltage(Tango::Attribute &attr) entering... "<< endl;
    
    try
    {
        SY2527_crate_proxy->command_inout("GetMeasVoltageOnChannel",position_in_crate,*attr_voltage_read);
        
        attr.set_value(attr_voltage_read);
        attr.set_quality(Tango::ATTR_VALID);
    }
    
    catch(Tango::DevFailed &df)
    {
        if(SY2527_crate_proxy!=0)
        {
            delete SY2527_crate_proxy;
            SY2527_crate_proxy = 0;
        }
        
        attr.set_value(attr_voltage_read);
        attr.set_quality(Tango::ATTR_INVALID);
        
        ERROR_STREAM<<df<<endl;
        throw df;
    }
}

//+----------------------------------------------------------------------------
//
// method : 		SY2527Channel::write_voltage
// 
// description : 	Write voltage attribute values to hardware.
//
//-----------------------------------------------------------------------------
void SY2527Channel::write_voltage(Tango::WAttribute &attr)
{
    DEBUG_STREAM << "SY2527Channel::write_voltage(Tango::WAttribute &attr) entering... "<< endl;
    
    attr.get_write_value(attr_voltage_write);
    
    vector <double> set_voltage_array;
    
    set_voltage_array.push_back(slotNumber);
    set_voltage_array.push_back(channelNumber);
    set_voltage_array.push_back(attr_voltage_write);
    
    try
    {
        SY2527_crate_proxy->command_in("SetVoltageOnChannel",set_voltage_array);
    }
    
    catch(Tango::DevFailed &df)
    {
        if(SY2527_crate_proxy!=0)
        {
            delete SY2527_crate_proxy;
            SY2527_crate_proxy = 0;
        }
        
        ERROR_STREAM<<df<<endl;
        throw df;
    }
}

//+------------------------------------------------------------------
/**
*	method:	SY2527Channel::power_on
*
*	description:	method to execute "PowerON"
*	preset voltage is applied on this channel
*
*
*/
//+------------------------------------------------------------------
void SY2527Channel::power_on()
{
    DEBUG_STREAM << "SY2527Channel::power_on(): entering... !" << endl;
    
    //	Add your own code to control device here
    
    try
    {
        SY2527_crate_proxy->command_in("PowerOnChannel",position_in_crate);
    }
    catch(Tango::DevFailed &df)
    {
        if(SY2527_crate_proxy!=0)
        {
            delete SY2527_crate_proxy;
            SY2527_crate_proxy = 0;
        }
        
        ERROR_STREAM<<df<<endl;
        throw df;
    }
}

//+------------------------------------------------------------------
/**
*	method:	SY2527Channel::power_off
*
*	description:	method to execute "PowerOFF"
*	preset voltage is no more applied on this channel
*
*
*/
//+------------------------------------------------------------------
void SY2527Channel::power_off()
{
    DEBUG_STREAM << "SY2527Channel::power_off(): entering... !" << endl;
    
    //	Add your own code to control device here
    try
    {
        SY2527_crate_proxy->command_in("PowerOffChannel",position_in_crate);
    }
    catch(Tango::DevFailed &df)
    {
        if(SY2527_crate_proxy!=0)
        {
            delete SY2527_crate_proxy;
            SY2527_crate_proxy = 0;
        }
        
        ERROR_STREAM<<df<<endl;
        throw df;
    }
}

//+------------------------------------------------------------------
/**
*	method:	SY2527Channel::dev_state
*
*	description:	method to execute "State"
*	This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
*
* @return	State Code
*
*/
//+------------------------------------------------------------------
Tango::DevState SY2527Channel::dev_state()
{
    DEBUG_STREAM << "SY2527Channel::dev_state(): entering... !" << endl;
    
    //	Add your own code to control device here
    
    std::stringstream s;
    Tango::DevState state_to_return;

    // concatenate internal message string 
    s << message << endl;
    
    s << "Slot Number : " + this->slotNumber << endl;
    s << "Channel Number : " + this->channelNumber << endl;

    Tango::DevUShort  status_byte;
    
    // try to get channel status byte
    try
    {
        SY2527_crate_proxy->command_inout("GetChannelStatus",position_in_crate,status_byte);
    }
    catch(Tango::DevFailed &df)
    {
        ERROR_STREAM<<df<<endl;
        
        if(SY2527_crate_proxy!=0)
        {
            delete SY2527_crate_proxy;
            SY2527_crate_proxy = 0;
        }
        
        s << "No Communication with SY2527Crate, try to init the SY2527Crate DS again"<<endl;
        
        set_status(s.str().c_str());
        return Tango::FAULT;
    }
    
    // Manage the returned state
    
    // FAULT State
    if( status_byte & 0x400)     // bit 10
    {
        s << "Channel is in calibration error"<<endl;
        set_status(s.str().c_str());
        return Tango::FAULT;
        
    }
    
    if( status_byte & 0x800)     // bit 11
    {
        s << "Channel is unplugged"<<endl;
        set_status(s.str().c_str());
        return Tango::FAULT;
    }
    
    // ALARM State
    if( status_byte & 0x08)     // bit 3
    {
        s << "Channel is over current"<<endl;
        set_status(s.str().c_str());
        return Tango::ALARM;
    }
    
    if( status_byte & 0x10)     // bit 4
    {
        s << "Channel is over voltage"<<endl;
        set_status(s.str().c_str());
        return Tango::ALARM;
    }
    
    if( status_byte & 0x20)     // bit 5
    {
        s << "Channel is under voltage"<<endl;
        set_status(s.str().c_str());
        return Tango::ALARM;
    }
    
    if( status_byte & 0x80)     // bit 7
    {
        s << "Channel is in max V"<<endl;
        set_status(s.str().c_str());
        return Tango::ALARM;
    }
        
    // NO corresponding state  because transient conditions 
    // this bit should not change Tango state 
    
    if( status_byte & 0x40)     // bit 6
    {
        s << "Channel is in external trip "<<endl;
    }
    
    if( status_byte & 0x100)     // bit 8
    {
        s << "Channel is external disable"<<endl;
    }
    
    if( status_byte & 0x200)     // bit 9
    {
        s << "Channel is in internal trip"<<endl;
    }

    // STANDBY State
    if( status_byte & 0x01)     // bit 0
    {
        s << "Channel is on"<<endl;
        state_to_return = Tango::STANDBY;

        set_status(s.str().c_str());
    }
    else
    {
        s << "Channel is off"<<endl;
        state_to_return = Tango::OFF;

        set_status(s.str().c_str());
    }	
    
    // RUNNING State
    if(status_byte & 0x02)     // bit 1
    {
        s << "Channel is ramping up"<<endl;
        state_to_return = Tango::RUNNING;

        set_status(s.str().c_str());
    }
    if( status_byte & 0x04)     // bit 2
    {
        s << "Channel is ramping down"<<endl;
        state_to_return = Tango::RUNNING;

        set_status(s.str().c_str());
    }

    return state_to_return;
}

}	//	namespace
